import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.glfw.GLFWWindowSizeCallback;
import org.lwjgl.opengl.GL;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.glfw.GLFW.glfwSetWindowSizeCallback;
import static org.lwjgl.glfw.GLFW.glfwShowWindow;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryUtil.NULL;

public class Main {
    private static long window;

    private static void loop() {

        // Устанавливаем цвет фона
        glClearColor(1.0f, 0.0f, 0.0f, 0.0f);

        //цикл будет выполняться до тех пор пока не будет нажата клавиша esc
        while (!glfwWindowShouldClose(window)) {
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // // очистка буфера цвета и глубины

            glfwSwapBuffers(window); // заменяет цветной буфер

             /*
              ПРОВЕРЯЕТ БЫЛИ ЛИ ВЫЗВАНЫ КАКИЕ-ТО СОБЫТИЯ (НАПРИМЕР: ВВОД С КЛАВИАТУРЫ, ПЕРЕМЕЩЕНИЕ МЫЩИ )
                   И  ВЫЗЫВАЕТ УСТАНОВЛЕННЫЕ ФУНКЦИИ (CALLBACK)
             */
            glfwPollEvents();
        }
    }

    public static void main(String[] args) {
        GLFWErrorCallback.createPrint(System.err).set();
        //// Инициализация GLFW
        if (!glfwInit())
            throw new IllegalStateException("Unable to initialize GLFW");
        glfwDefaultWindowHints();

        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);//ЗАДАЕТ ВОЗМОЖНОСТЬ ИЗМЕНЕНИЯ РАЗМЕРА ОКНА

        int width = 640;
        int height = 480;
        window = glfwCreateWindow(width, height, "Урок по OPEN GL", NULL, NULL);//Создание окна
        if (window == NULL)
            throw new RuntimeException("Ошибка создания GLFW окна");

        // если нажата клавиша esc glfwSetWindowShouldClose вернет true и бесконечный цикл завершится
        glfwSetKeyCallback(window, (window, key, scancode, action, mods) -> {
            if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
                glfwSetWindowShouldClose(window, true); // We will detect this in the rendering loop
            }
        });

        // возвращает поддерживаемый видеорежим
        GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());

        glfwSetWindowPos(//центрируем наше окно
                window,
                (vidmode.width() - width) / 2,
                (vidmode.height() - height) / 2
        );
        //делаем окно текущим контекстом
        glfwMakeContextCurrent(window);
        // Включить v-sync
        glfwSwapInterval(1);
        //Сделать окно видимым
        glfwShowWindow(window);
        //делает привязвку LWJGL с Open GL
        GL.createCapabilities();
        //glViewport переводит из мировых координат (-1...1) в экранные представление open gl
        glfwSetWindowSizeCallback(window, new GLFWWindowSizeCallback() {
            @Override
            public void invoke(long window, int width, int height) {
                glViewport(0, 0, width, height);
            }
        });
        loop();
    }
}
